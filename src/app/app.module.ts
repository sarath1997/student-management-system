import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { StudentModule } from './student/student.module';

import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './home/aboutus/aboutus.component';
import { HeadComponent } from './home/head/head.component';
import { LoginComponent } from './home/login/login.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AuthorizationService } from './authorization.service';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutusComponent,
    HeadComponent,
   
    LoginComponent,
    
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AdminModule,
    StudentModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS,
    useClass:AuthorizationService,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
