import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { MarksService } from 'src/app/admin/marks.service';
import { AuthorizationService } from 'src/app/authorization.service';
import { LoginService } from 'src/app/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  router:any[];

  constructor( private route:Router ,private fs:HttpClient,private ls:LoginService,private x:MarksService) { }

  ngOnInit() {
    
  }
    
  

  submitData(value)
  {
    console.log(value);

if(value.user=='admin'){
  this.ls.doAdminLogin(value).subscribe(res=>{
    console.log(res);
    if(res['message']==='Invalid Username')
    {
      alert('Invalid Username');
      this.route.navigate(['/login'])
    }
    if(res['message']==='Invalid Password')
    {
      alert('Invalid Password');
      this.route.navigate(['/login'])
    }
    if(res['message']==='success')
    {
      alert(res['message']);
      localStorage.setItem('idToken',res['token']);
      this.route.navigate(['/testadmin']);
    }
  });
}
else if(value.user=="student"){
  this.ls.doStudentLogin(value).subscribe(res=>{
    console.log(res);
    if(res["message"]==="Invalid Username")
    {
      alert("Invalid Username");
      this.route.navigate(['/login']);
    }
    if(res["message"]==="Invalid Password")
    {
      alert("Invalid Password");
      this.route.navigate(['/login']);
    }
    if(res["message"]==="success")
    {
      alert(res["message"]);
      localStorage.setItem("idToken",res['token']);
      console.log(res['data']);
      this.x.loggedUser(res['data']);
      this.route.navigate(['/student']);
    }
  });
}
  if(value.admin==""){
    alert("please enter username and password");
  }

  }
 /* if(value.username=="admin"){

  
  this.route.navigate(["testadmin"]);
  }
  else
  {
    this.route.navigate(["student"]);
  }
  */
}



