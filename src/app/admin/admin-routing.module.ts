import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestadminComponent } from './testadmin/testadmin.component';
import { Testadmin1Component } from './testadmin1/testadmin1.component';
import { Testadmin2Component } from './testadmin2/testadmin2.component';
import { AttendenceComponent } from './attendence/attendence.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { LoginComponent } from '../home/login/login.component';
import { StudentsprofileComponent } from './admin/studentsprofile/studentsprofile.component';
import { LeaveComponent } from './admin/leave/leave.component';
import { LogoutComponent } from './logout/logout.component';
import { AddstudentComponent } from './addstudent/addstudent.component';

const routes: Routes = [{
  path:"login",
  component:LoginComponent
},

  
  {
    path:'testadmin',
    component:TestadminComponent,
    children:[{path:'postmarks',component:Testadmin1Component},
    {path:'profile',component:StudentsprofileComponent},
    {path:'feestatus',component:Testadmin2Component},{path:'attendence',component:AttendenceComponent},
    {path:'notifications',component:NotificationsComponent},{path:'leave',component:LeaveComponent},
    {path:'logout',component:LogoutComponent},{path:'addstudent',component:AddstudentComponent}]
  },
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
