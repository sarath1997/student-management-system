import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostmarksService {

  constructor(private x:HttpClient) { }

  adminpost():Observable<any[]>
  {
    return this.x.get<any[]>('testadmin/postmarks');
  }

}
