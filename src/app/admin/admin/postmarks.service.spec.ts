import { TestBed } from '@angular/core/testing';

import { PostmarksService } from './postmarks.service';

describe('PostmarksService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostmarksService = TestBed.get(PostmarksService);
    expect(service).toBeTruthy();
  });
});
