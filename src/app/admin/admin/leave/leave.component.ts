import { Component, OnInit } from '@angular/core';
import { MarksService } from '../../marks.service';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router';

@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.css']
})
export class LeaveComponent implements OnInit {
  data:any[]=[];
 loggeduser; 
  
  constructor(private x:MarksService,private hc:HttpClient,private route:Router) { }

  ngOnInit() {
    this.hc.get('/admin/leaverequest').subscribe(data=>{
      if(data['message']=="unauthorized access")
      {
        alert("unauthorized access")
        
          this.route.navigate(['/home/login']);
      }
        
        else{
          this.data=data['message'];
        }
      
     
    })
  }
  accept(rno)
  {
    this.loggeduser=this.x.sendLoggedUser();
    this.hc.post('/admin/savereq',({"message":"request is accepted","rno":rno})).subscribe((res)=>{
      alert(res['message'])
      })
    }
    reject(rno)
    {
      this.loggeduser=this.x.sendLoggedUser();
      this.hc.post('admin/savereq',({"message":"request is rejected","rno":rno})).subscribe((res)=>{
        alert(res['message'])
      })
    }
  }

