import { Component, OnInit } from '@angular/core';
import { MarksService } from '../../marks.service';

@Component({
  selector: 'app-studentsprofile',
  templateUrl: './studentsprofile.component.html',
  styleUrls: ['./studentsprofile.component.css']
})
export class StudentsprofileComponent implements OnInit {
m:any[];
data:any[];
  constructor(private x:MarksService) { }

  ngOnInit() {
    
    this.x.sendingToProfile().subscribe(data=>{
      this.data=data['message'];
     })
    
  
  }

}
