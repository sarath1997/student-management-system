import { Component, OnInit } from '@angular/core';
import { MarksService } from '../marks.service';
import {HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';



@Component({
  selector: 'app-testadmin1',
  templateUrl: './testadmin1.component.html',
  styleUrls: ['./testadmin1.component.css']
})
export class Testadmin1Component implements OnInit {
a:any[]=[];
courses:string[]=['MBA','MCA','B.Tech','M.Tech'];
branches:string[]=['Finance','HR','ECE','EEE','Civil'];
  data: any[];
constructor(private x:MarksService, private rf:HttpClient ,private route:Router){}
ngOnInit() 
  {
    this.x.sendDataToStudent().subscribe(data=>{
      if(data['message']=="unauthorized access")
    {
      alert("unauthorized access")
      
        this.route.navigate(['/home/login']);
    }
      
      else{
        
      this.data=data['message'];
      }
    })
  }
marksData(value)
{
  
  this.a.push (value);
  this.x.receiveData(this.a);
  console.log(value)
  console.log(this.a)
  this.rf.post('testadmin/postmarks',value).subscribe(res=>{
    alert(res['message']);
  
  })
}
b:boolean=false;
objectToModify:object;
editMarks(obj)
{
  this.objectToModify=obj;
  console.log(this.objectToModify);
  this.b=true;
}
testing(value)
{
  this.b=false;
  this.rf.put('admin/updatemarks',value).subscribe(res=>{
    if(res['message']=="updated")
    {
      alert("successfully modified");
    }
  })
}

deleteRecord(no)
        {
          this.b=false;
          console.log(no);
          this.rf.delete(`/admin/deletemarks/${no}`).subscribe(res=>{
            alert(res['message']);
            this.data=res['data']
          });
        }
      }
     
  
  


