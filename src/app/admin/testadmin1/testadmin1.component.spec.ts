import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Testadmin1Component } from './testadmin1.component';

describe('Testadmin1Component', () => {
  let component: Testadmin1Component;
  let fixture: ComponentFixture<Testadmin1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Testadmin1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Testadmin1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
