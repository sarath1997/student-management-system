import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MarksService {
   constructor(private hc:HttpClient) { }
  
   c:any;
   f:any;
   i:any;
  
   j:any;
   user;
  m:object[]=[];
  receiveData(value)
  
  {
    this.m=value;
  console.log(this.m)
  }
  sendDataToStudent():Observable<any[]>

  {
  
    return this.hc.get<any[]>('student/marks');
  }
  

  d:any[]=[];

  receiveDataToStudent(a)
  {
    this.d=a;
  }


  sendingDataFromStudent():Observable<any[]>

  {
    return this.hc.get<any[]>('student/attendence');
  }
  b:any[];
  receiveDataToStudent1(a)
  {
     this.b=a;
  }
  sendData1()
  {
   return this.b;
  }
  n:number;
  receivepending(pending)
  {
    this.n=pending;
  }
    
  
  sendPending():Observable<any[]>

  
  {
    return this.hc.get<any[]>('student/fee');
  }
s:any[];
receiveFromProfile(g)
{
  this.s=g;
}
sendingToProfile():Observable<any[]>

{

  return this.hc.get<any[]>('student/profile');
}
h:any[];
sendTostudentNotification(a)
{
  this.h=a;
}
receivedData():Observable<any[]>
{
  return this.hc.get<any[]>('student/notifications');
}
receiveLeave(a)
{
  this.c=a;

}
sendLeave():Observable<any[]>
{
  return this.hc.get<any[]>('admin/leave');
}
receiveAccept(e)
{
  this.f=e;
}
sendAccept()
{
  return this.f;
}
receiveReject(h)
{
  this.i=h;
}
sendReject()
{
  return this.i;
}
loggedUser(user)
{
  console.log(user);
  this.user=user[0];
}
sendLoggedUser()
{
  return this.user;
}
specificmarks(user)
{
  console.log(user);
  return this.hc.post('/student/specificstnmarks',user);
}
specificattendence(user)
{
  console.log(user);
  return this.hc.post('/student/specificstnattendence',user);
}
specificfees(user)
{
  console.log(user);
  return this.hc.post('/student/specificstnfees',user);
}
specificprofile(user)
{
  console.log(user);
  return this.hc.post('/student/specificstnprofile',user);
}
specificleaverequest(user)
{
  console.log(user);
  return this.hc.post<any[]>('student/specificleaverequest',user);
}
}