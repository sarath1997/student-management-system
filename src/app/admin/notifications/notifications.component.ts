import { Component, OnInit } from '@angular/core';
import { MarksService } from '../marks.service';
import {HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
a:any;
  constructor(private x:MarksService, private ns:HttpClient) { }

  ngOnInit() {
    
  }
notifications(value)
{
  this.a=value;
  this.x.sendTostudentNotification(this.a);
  this.ns.post('admin/notifications',value).subscribe(res=>{
   alert(res['message']);
  })
}
}
