import { Component,OnInit } from '@angular/core';
import { MarksService } from '../marks.service';
import {HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-testadmin2',
  templateUrl: './testadmin2.component.html',
  styleUrls: ['./testadmin2.component.css']
})
export class Testadmin2Component  {
  a:any[]=[];
  pending:number;
  j:number;
  courses:string[]=['MBA','MCA','B.Tech','M.Tech'];
  branches:string[]=['Finance','HR','ECE','EEE','Civil'];
  data: any[]=[];
  constructor(private x:MarksService, private fp:HttpClient,private router:Router) {}
ngOnInit(){
  this.x.sendPending().subscribe(data=>{
    if(data['message']=="unauthorized access")
    {
      alert("unauthorized access")
      
        this.router.navigate(['/home/login']);
    }
      
      else{
        this.data=data['message'];
      }
    
   
  })
}
feesData(value)
{
  this.a.push (value);
  this.x.receiveData(this.a);
  console.log(value)
  console.log(this.a)
  this.pending=value.total-value.paid;

  this.x.receivepending(this.pending);
this.x.sendPending();
  this.fp.post('admin/feestatus',value).subscribe(res=>{
    alert(res['message']);
  })
}
b:boolean=false;
objectToModify:object;
editingFeestatus(obj)
{
  this.objectToModify=obj;
  console.log(this.objectToModify);
  this.b=true;
}
testing(value)
{
  this.b=false;
  this.fp.put('admin/updatefeestatus',value).subscribe(res=>{
    if(res['message']=="updated")
    {
      alert("successfully modified");
    }
  })
}
deleteRecord(no)
        {
          this.b=false;
          console.log(no);
          this.fp.delete(`/admin/deletestudentfee/${no}`).subscribe(res=>{
            alert(res['message']);
            this.data=res['data']
          });
        }
      }


