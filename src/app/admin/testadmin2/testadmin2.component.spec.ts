import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Testadmin2Component } from './testadmin2.component';

describe('Testadmin2Component', () => {
  let component: Testadmin2Component;
  let fixture: ComponentFixture<Testadmin2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Testadmin2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Testadmin2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
