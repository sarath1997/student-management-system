import { Component } from '@angular/core';
import { MarksService } from '../marks.service';
import {HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent  {
  p:any[]=[];
  courses:any[]=['MBA','MCA','B.Tech','M.Tech'];
  branchs:any[]=['Finance','HR','ECE','EEE','Civil'];
  states:any[]=['AP','Telangana','Bngalore'];

  
  
  
    constructor(private x:MarksService,private fs:HttpClient,private route:Router ) {}
     g:any[];
     data:any[];
     ngOnInit(){
      this.x.sendingToProfile().subscribe(data=>{
        this.data=data['message'];
         
       })
     }
      profileData(value)
      
       {
         this.g=value;
         this.x.receiveFromProfile(this.g);
         this.fs.post('admin/addstudent',value).subscribe(res=>{
           alert(res['message']);
          
          })
        }
        b:boolean=false;
        objectToModify:object;
        editingProfile(obj)
        {
          this.objectToModify=obj;
          console.log(this.objectToModify);
          this.b=true;
        }
        testing(value)
        {
          this.b=false;
          this.fs.put('admin/updateprofile',value).subscribe(res=>{
            if(res['message']=="updated")
            {
              alert("successfully modified");
            }
          })
        }
        deleteRecord(rno)
        {
          this.b=false;
          console.log(rno);
          this.fs.delete(`/admin/deletestudent/${rno}`).subscribe(res=>{
            alert(res['message']);
            this.data=res['data']
          });
        }
      }
      
        
