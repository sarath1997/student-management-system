import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';

import { AdminComponent } from './admin/admin.component';
import { FormsModule } from '@angular/forms';
import { TestadminComponent } from './testadmin/testadmin.component';
import { Testadmin1Component } from './testadmin1/testadmin1.component';
import { Testadmin2Component } from './testadmin2/testadmin2.component';
import { AttendenceComponent } from './attendence/attendence.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { LogoutComponent } from './logout/logout.component';
import { StudentsprofileComponent } from './admin/studentsprofile/studentsprofile.component';
import { LeaveComponent } from './admin/leave/leave.component';
import { AddstudentComponent } from './addstudent/addstudent.component';


@NgModule({
  declarations: [ AdminComponent, TestadminComponent, Testadmin1Component, Testadmin2Component, AttendenceComponent, NotificationsComponent, LogoutComponent, StudentsprofileComponent,LeaveComponent, AddstudentComponent, ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    
    
  ]
})
export class AdminModule { }
