import { Component,OnInit} from '@angular/core';
import { MarksService } from '../marks.service';
import {HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-attendence',
  templateUrl: './attendence.component.html',
  styleUrls: ['./attendence.component.css']
})
export class AttendenceComponent implements OnInit {
 
d:any[]=[];
courses:string[]=['MBA','MCA','B.Tech','M.Tech'];
branches:string[]=['Finance','HR','ECE','EEE','Civil'];
  data: any[]=[];
  constructor(private x:MarksService, private ad:HttpClient,private route:Router) { }
 

    ngOnInit(){
    this.x.sendingDataFromStudent().subscribe(data=>{
      if(data['message']=="unauthorized access")
    {
      alert("unauthorized access")
      
        this.route.navigate(['/home/login']);
    }
      
      else{
        this.data=data['message'];
      }
    
   
  })
}
  attendenceData(value)
  {
    
    this.d.push(value);
    this.x.receiveData(this.d);
    console.log(value)
    console.log(this.d);
    this.ad.post('admin/attendence',value).subscribe(res=>{
      alert(res['message']);
    })
  }
  b:boolean=false;
objectToModify:object;
editingAttendence(obj)
{
  this.objectToModify=obj;
  console.log(this.objectToModify);
  this.b=true;
}
testing(value)
{
  this.b=false;
  this.ad.put('admin/updateattendence',value).subscribe(res=>{
    if(res['message']=="updated")
    {
      alert("successfully modified");
    }
  })
}

deleteRecord(name)
        {
          console.log(name);
          this.ad.delete(`/admin/delete/${name}`).subscribe(res=>{
            alert(res['message']);
            this.data=res['data'];
          });
        }
  }
  
  
  


