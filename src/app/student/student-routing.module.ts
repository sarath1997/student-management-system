import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { ProfileComponent } from './student/profile/profile.component';
import { StudentComponent } from './student/student.component';
import { MarksComponent } from './student/marks/marks.component';
import { AttendenceComponent } from './student/attendence/attendence.component';
import { FeestatusComponent } from './student/feestatus/feestatus.component';
import { NotificationsComponent } from './student/notifications/notifications.component';

import { LogoutComponent } from './student/logout/logout.component';
import { LeaveComponent } from './student/leave/leave.component';




const routes: Routes = [{
  path:"student",
  component:StudentComponent,
  children:[{
    path:"marks",
    component:MarksComponent
  },{
    path:"fee",
    component:FeestatusComponent},{
      path:"notifications",
      component:NotificationsComponent},{
        path:"attendence",
        component:AttendenceComponent},{
          path:"profile",
          component:ProfileComponent},{path:"logout",component:LogoutComponent},{path:"leave",component:LeaveComponent}]

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
