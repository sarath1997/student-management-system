import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { StudentComponent } from './student/student.component';
import { FormsModule } from '@angular/forms';

import { MarksComponent } from './student/marks/marks.component';
import { AttendenceComponent } from './student/attendence/attendence.component';
import { FeestatusComponent } from './student/feestatus/feestatus.component';
import { LogoutComponent } from './student/logout/logout.component';
import { ProfileComponent } from './student/profile/profile.component';
import { NotificationsComponent } from './student/notifications/notifications.component';
import { LeaveComponent } from './student/leave/leave.component';



@NgModule({
  declarations: [StudentComponent, MarksComponent, FeestatusComponent,
     AttendenceComponent,NotificationsComponent , ProfileComponent, LogoutComponent, LeaveComponent],

  imports: [
    CommonModule,
    StudentRoutingModule,
    FormsModule
  ]
})
export class StudentModule { }
