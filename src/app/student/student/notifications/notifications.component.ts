import { Component, OnInit } from '@angular/core';
import { MarksService } from 'src/app/admin/marks.service';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
notifications:any;
  data: any;
  constructor(private x:MarksService) { }

  ngOnInit() {
    this.notifications=this.x.receivedData().subscribe(data=>{
      this.data=data['message'];
    })
  }
}
