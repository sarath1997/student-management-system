import { Component,OnInit } from '@angular/core';
import { MarksService } from 'src/app/admin/marks.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
m:any[];
data:any[];
user;
  constructor(private x:MarksService,private router:Router) {}
   
   ngOnInit(){
     this.user=this.x.sendLoggedUser();
     console.log(this.user)
     this.x.specificprofile(this.user).subscribe(profile=>{
      if(profile['message']=="unauthorized access")
      {
        alert(profile['message']);
        this.router.navigate(['/home/login']);
      }
      else{
        
      
       this.m=profile['message'];
       console.log(this.m)
      }
     })
    
     }
   }

  


