import { Component, OnInit } from '@angular/core';
import { MarksService } from 'src/app/admin/marks.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css'],
})
export class MarksComponent implements OnInit {
  data:any[];
  

  constructor(private x:MarksService,private router:Router) { }

  y:any[]=[];
  user;
  ngOnInit() 
  {
    this.user=this.x.sendLoggedUser();
    this.x.specificmarks(this.user).subscribe(marks=>{
      if(marks['message']=="unauthorized access")
      {
        alert(marks['message']);
        this.router.navigate(['/home/login']);
      }
      else{
        
      
      this.y=marks['message'];
      console.log(this.y)
      }
    })
  }
}
