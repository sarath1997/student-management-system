import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  constructor(private route:Router) { }

  ngOnInit() {
  }
  submitData(value)
  {
    if(value.username=="student")
    {
      this.route.navigate(["student"]);
    }
    else
    {
      this.route.navigate(["admin"]);
    }
  }
}
