import { Component, OnInit } from '@angular/core';
import { MarksService } from 'src/app/admin/marks.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-feestatus',
  templateUrl: './feestatus.component.html',
  styleUrls: ['./feestatus.component.css']
})
export class FeestatusComponent implements OnInit {
y:any[]=[];
  data: any[];
user;
  constructor(private x:MarksService,private router:Router) { }


  ngOnInit() {
    this.user=this.x.sendLoggedUser();
    this.x.specificfees(this.user).subscribe(fees=>{
      if(fees['message']=="unauthorized access")
      {
        alert(fees['message']);
        this.router.navigate(['/home/login']);
      }
      else{
        
      
      this.y=fees['message'];
      console.log(this.y)
      }
    })
  }

}
