import { Component, OnInit } from '@angular/core';
import { MarksService } from 'src/app/admin/marks.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-attendence',
  templateUrl: './attendence.component.html',
  styleUrls: ['./attendence.component.css']
})
export class AttendenceComponent implements OnInit {
  data: any[];

  constructor(private x:MarksService,private router:Router) { }

  t:any[]=[];
  user;
  ngOnInit() {
    this.user=this.x.sendLoggedUser();
    this.x.specificattendence(this.user).subscribe(attendence=>{
      if(attendence['message']=="unauthorized access")
      {
        alert(attendence['message']);
        this.router.navigate(['/home/login']);
      }
      else{
        
      
      this.t=attendence['message'];
      console.log(this.t)
      }
    })
   
  }
}
