import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './home/aboutus/aboutus.component';
import { AdminComponent } from './admin/admin/admin.component';
import { StudentComponent } from './student/student/student.component';
import { TestadminComponent } from './admin/testadmin/testadmin.component';
import { HeadComponent } from './home/head/head.component';


import { LoginComponent } from './home/login/login.component';
import { LogoutComponent } from './admin/logout/logout.component';
import { StudentsprofileComponent } from './admin/admin/studentsprofile/studentsprofile.component';



const routes: Routes = [
  {
    path:'',
    redirectTo:'home',
    pathMatch:"full"
  },
  {
    path:'home',
    component:HomeComponent,
    children:[{
      path:'',
      redirectTo:'head',
      pathMatch:'full'
    },
    {
      path:'head',
    component:HeadComponent
    },
    {
      path:'aboutus',
    component:AboutusComponent,
    },
    {
      path:'login',
      component:LoginComponent
    },
    {
      path:'admin',
      component:LoginComponent
    },
    {
      path:'logout',
      component:LogoutComponent
    },
    {
      path:'notifications',
      component:StudentsprofileComponent
    }
      
    
  ]
},

  {
    path:'head',
    component:HeadComponent
  },
  {
    path:'testadmin',
    component:TestadminComponent
  },
  {
    path:'aboutus',
    component:AboutusComponent
  
  },
  {
    path:'admin',
    component:AdminComponent
  },
  {
      
    path:'student',
    component:StudentComponent
  }
  
   
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
