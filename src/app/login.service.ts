import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService{
//to check login status of user
isLoggedin:boolean=false;
  constructor( private hc:HttpClient)  {}

doAdminLogin(userObject):Observable<any[]>
{
  return this.hc.post<any[]>('admin/adminlogin',userObject);
}
doStudentLogin(userObject):Observable<any[]>
{
  return this.hc.post<any[]>('student/studentlogin',userObject);
}
}
