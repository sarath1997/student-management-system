//create express application
const exp=require('express');
const app=exp();

//import and use bodyparser
const bodyparser=require("body-parser")
app.use(bodyparser.json());
//import path module
var path=require("path");
//import adminroutes
const adminroutes=require('./routes/adminroutes');

//import studentroutes
const studentroutes=require('./routes/studentroutes');
//informing to server about routes
app.use("/admin",adminroutes);
app.use("/testadmin",adminroutes);
app.use("/student",studentroutes);
app.use("/home",studentroutes);
app.use((err,req,res,next)=>{
    console.log(err);
})
//connecting angular application with server
app.use(exp.static(path.join(__dirname,'../dist/project2/')));
//assigning port number
const portno=2000;
app.listen(process.env.PORT || 8080,()=>{
    console.log('server is running on port 8080')
})
