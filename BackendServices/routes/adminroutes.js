//create a express mini app to handle admin routes
const exp=require('express');
var adminroutes=exp.Router();
const bcrypt=require('bcrypt');
const nodemailer=require('nodemailer')
const jwt=require("jsonwebtoken");

//secret key
var secretkey="secret"
//import DBConfig.js
const getDb=require('../DBConfig').getDb;
const initDb=require('../DBConfig').initDb;

initDb();
 //import middleware
        const checkAuthorization=require('../Middleware/checkAuthorization'); 
        //postfees  
        
    adminroutes.post("/feestatus",checkAuthorization,(req,res)=>{


        console.log(req.body);
        dbo=getDb();
        dbo.collection("feescollection").insert(req.body,(err,success)=>{
            if(err)
            {
                console.log("error in admin login")
            }
            else{
                res.json({"message":"fees successfully"})
            }
        })
        });
        //admin marks update

adminroutes.put("/updatefeestatus",(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    
    dbo.collection("feescollection").update({name:{$eq:req.body.name}},{$set:{no:req.body.no}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            console.log(success);
            res.json({message:"updated"})
    
        }
    })
});
adminroutes.put('/modify',(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    //run update
    dbo.collection('feescollection').update({name:{$eq:req.body.name}},{$set:{no:req.body.no}},(err,success)=>{
        if(err)
        {
            next(err);

        }
        else{
            res.json({message:"success"})
        }
    })
});
adminroutes.delete('/deletestudentfee/:no',(req,res,next)=>{
    console.log('hi delete')
    console.log(req.body);
    //delete document with name as "req.params.name"
    dbo=getDb();
    dbo.collection('feescollection').deleteOne({rno:{$eq:req.body.no}},(err,result)=>{
        if(err)
        {
            next(err);
    
        }
        else{
            //read remaining data and send to client
            dbo.collection('feescollection').find().toArray((err,success)=>{
                if(err)
                {
                next(err);
                }
                else{
                    console.log(success)
                    res.json({message:"Record deleted",data:success})
                }
    
            })
        }
    })
    });

    
        
                 
    
adminroutes.post('/leaverequest',checkAuthorization,(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    if(req.body=={})
    {
res.json({message:"server did not receive data"})
    }
    else{
        dbo.collection("leavecollection").insertOne(req.body,(err,dataArray)=>{
            if(err){
                next(err)
            }
            else{
                dbo.collection("leavecollection").deleteOne({rno:{$eq}},(err,dataArray)=>{
                    if(err)
                    {
                        next(err)
                    }
                    else{
                        res.json({message:"successfully inserted"})
                    }
                })
            }
        })
    }
    });
adminroutes.get('/leaverequest',(req,res,next)=>{
    dbo=getDb();
    dbo.collection("leavecollection").find().toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
        }
    })
})
adminroutes.post('/savereq',(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    if(req.body=={})
    {
res.json({message:"server did not receive data"})
    }
    else{
        dbo.collection("responce").insertOne(req.body,(err,dataArray)=>{
            if(err){
                next(err)
            }
            else{
                dbo.collection("leavecollection").deleteOne({rno:{$eq:req.body.rno}},(err,dataArray)=>{
                    if(err)
                    {
                        next(err)
                    }
                    else{
                        res.json({message:"successfully inserted"})
                    }
                })
            }
        })
    }
    });


//admin route handlers

adminroutes.get('/read',(req,res,)=>{
    dbo=getDb();
    res.json({message:"admin read working"});
})



adminroutes.post('/addstudent',checkAuthorization,(req,res,next)=>{
    
    const transport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'amulyakasam@gmail.com',
            pass: '8790871678',
        },
    });
    const mailOptions = {
        from: 'amulyakasam@gmail.com',
        to: req.body.gmail,
        subject: 'student credentials',
       // html: 'hello world!'
       text:`rollnumber:${req.body.rno},password:${req.body.pass}`
    };
    transport.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
        }
    });
    
    
        

console.log(req.body);
dbo=getDb();
dbo.collection('detailscollection').find({user:{$eq:req.body.user}}).toArray((err,result)=>{
    if(result=="")
    {

    
//hash the password req.body.password
bcrypt.hash(req.body.pass,6,(err,hashedpassword)=>{
    if(err)
    {
        next(err);
    }
    else{
        //replace plain text password with hashed password
        req.body.pass=hashedpassword;
    
dbo.collection('detailscollection').insert(req.body,(err,success)=>{
    if(err)
    {
        console.log("error in admin post")
    }
    else{
        res.json({"message":"add student successfully"})
    }
})
    }

})
}
else{
    res.json({"message":"duplicate"});
    console.log("duplicate",result)
}
})
});
//addstudent delete

adminroutes.delete('/deletestudent/:rollno',(req,res,next)=>{
    console.log(req.params);
    //delete document with name as "req.params.rollno"
    dbo=getDb();
    dbo.collection("detailscollection").deleteOne({rollno:{$eq:req.params.rno}},(err,success)=>{
        if(err)
        {
            next(err);
    
        }
        else{
            //read remaining data and send to client
            dbo.collection('detailscollection').find().toArray((err,success)=>{
                if(err)
                {
                next(err);
                } 
                else{
                    res.json({message:"Record deleted",data:success})
                }
    
            })
        }
    })
    });
    //profile put
adminroutes.put('/updateprofile',(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("detailscollection").update({stn:{$eq:req.body.stn}},{$set:{rno:req.body.rno}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            console.log(success);
            res.json({"message":"updated"});
        }
    })
});
//profile put
adminroutes.put('/modify',(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    //run update
    dbo.collection('detailscollection').update({stn:{$eq:req.body.stn}},{$set:{rno:req.body.rno}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            res.json({"message":"success"})
        }
    })
});

//import bodyparser
var bodyparser=require("body-parser");
adminroutes.use(bodyparser.json());

adminroutes.post('/adminlogin',(req,res)=>{


    console.log(req.body);
    dbo=getDb();
    dbo.collection('admin1collection').find({username:{$eq:req.body.username}}).toArray((err,userArray)=>{
        console.log(userArray.length);
        if(userArray.length==0)
        {
            res.json({message:"Invalid Username"});
        }
        else if(userArray[0].password!==req.body.password)
        {

            res.json({message:"Invalid Password"})
        }
        else{
        
            const signedtoken=jwt.sign({name:userArray[0].name},secretkey,{expiresIn:"7d"});
            console.log(signedtoken);
           console.log(userArray);
            res.json({message:"success",token:signedtoken});
            
        }
    })
    });
    
//postmarks
   adminroutes.post('/postmarks',checkAuthorization,(req,res,next)=>{


    console.log(req.body);
    dbo=getDb();
    
    dbo.collection('postingmarkscollection').insert(req.body,(err,success)=>{
        if(err)
        {
            console.log("error in admin login")
        }
        else{
            dbo.collection('postingmarkscollection').find().toArray((err,dataArray)=>{
                if(err)
                {
                    next(err);
                }
                else
                {
                    res.json({message:"posted successfully",data:dataArray});
                }
            })
        }
    })
    });
    //admin marks update

adminroutes.put("/updatemarks",(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    
    dbo.collection("postingmarkscollection").update({name:{$eq:req.body.name}},{$set:{no:req.body.no}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            console.log(success);
            res.json({message:"updated"})
    
        }
    })
});
adminroutes.put('/modify',(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    //run update
    dbo.collection('postingmarkscollection').update({name:{$eq:req.body.name}},{$set:{no:req.body.no}},(err,success)=>{
        if(err)
        {
            next(err);

        }
        else{
            res.json({message:"success"})
        }
    })
});
adminroutes.delete('/deletemarks/:no',(req,res,next)=>{
    console.log('hi delete')
    console.log(req.body);
    //delete document with name as "req.params.name"
    dbo=getDb();
    dbo.collection('postingmarkscollection').deleteOne({rno:{$eq:req.body.no}},(err,result)=>{
        if(err)
        {
            next(err);
    
        }
        else{
            //read remaining data and send to client
            dbo.collection('postingmarkscollection').find().toArray((err,success)=>{
                if(err)
                {
                next(err);
                }
                else{
                    console.log(success)
                    res.json({message:"Record deleted",data:success})
                }
    
            })
        }
    })
    });

    
  
 //post attendence
adminroutes.post('/attendence',checkAuthorization,(req,res)=>{
 console.log(req.body);
 dbo=getDb();
 dbo.collection('attendencecollection').insert(req.body,(err,success)=>{
 if(err)
     {
    console.log("error in admin login")
 }
 else{
  res.json({"message":"attendence success"})
  }
 })
});
adminroutes.put("/updateattendence",(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    
    dbo.collection("attendencecollection").update({name:{$eq:req.body.name}},{$set:{no:req.body.no}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            console.log(success);
            res.json({message:"updated"})
    
        }
    })
});
adminroutes.put('/modify',(req,res,next)=>{
    console.log(req.body);
    dbo=getDb();
    //run update
    dbo.collection('attendencecollection').update({name:{$eq:req.body.name}},{$set:{no:req.body.no}},(err,success)=>{
        if(err)
        {
            next(err);

        }
        else{
            res.json({message:"success"})
        }
    })
});
        adminroutes.delete('/delete/:no',(req,res,next)=>{
            console.log(req.body);
            //delete document with name as "req.params.rollno"
            dbo=getDb();
            dbo.collection("attendencecollection").deleteOne({rno:{$eq:req.body.no}},(err,success)=>{
                if(err)
                {
                    next(err);
            
                }
                else{
                    //read remaining data and send to client
                    dbo.collection('attendencecollection').find().toArray((err,success)=>{
                        if(err)
                        {
                        next(err);
                        } 
                        else{
                            res.json({message:"Record deleted",data:success})
                        }
            
                    })
                }
            })
            });
         
    
//post notification
 adminroutes.post('/notifications',(req,res)=>{


                console.log(req.body);
                dbo=getDb();
                dbo.collection('notificationscollection').insert(req.body,(err,success)=>{
                    if(err)
                    {
                        console.log("error in admin login")
                    }
                    else{
                        res.json({"message":"notification success"})
                    }
                })
                });
       
module.exports=adminroutes;