//create a express mini app to handle student routes
const exp=require('express');
var studentroutes=exp.Router();
const bcrypt=require('bcrypt');
const jwt=require("jsonwebtoken");
//import DBConfig.js file
const getDb=require('../DBConfig').getDb;
const initDb=require('../DBConfig').initDb;

//first call initDb function
initDb();
 const checkAuthorization=require('../Middleware/checkAuthorization'); 
//secret key
var secretkey="secret"

studentroutes.post('/specificstnprofile',checkAuthorization,(req,res)=>{
    dbo=getDb();
    console.log(req.body)
    dbo.collection('detailscollection').find({rno:{$eq:req.body.rno}}).toArray((err,success)=>{
        if(err)
        {
            console.log('error in data reading');
            console.log(err);
        }
       
        else{
            console.log(success)
            res.json({"message":success});
           
        }
    })
});

studentroutes.get('/profile',(req,res)=>{

dbo=getDb();
dbo.collection('detailscollection').find().toArray((err,success)=>{
    if(err)
    {
        console.log("error in data reading");
        console.log(err);
    }
    else if(success.length==0)
    {
        res.json({"message":"no data found"});
    }
    else
    {
        res.json({"message":success});
    }
})
});

/*
studentroutes.post('/studentlogin',(req,res)=>{
 console.log(req.body);
    dbo=getDb();
    dbo.collection('detailscollection').find({user:{$eq:req.body.username}}).toArray((err,success)=>{
        console.log(success.length);
        if(success.length==0)
        {
            res.json({message:"Invalid Username"});
        }
        else if(success[0].password!==req.body.pass)
        {
            res.json({message:"Invalid Password"});
        }
        else{
            res.json({message:"success"});
        }
    })
    });*/
    studentroutes.post('/studentlogin',(req,res,next)=>{
        console.log(req.body);
        dbo=getDb();
        dbo.collection('detailscollection').find({user:{$eq:req.body.username}}).toArray((err,userArray)=>{
            console.log(userArray);
            console.log(req.body.password)
            if(userArray.length===0){
                res.json({message:"Invalid Username"})
            }

            else{
                //comparing plain text password with hashed password
                console.log(req.body.password)
                bcrypt.compare(req.body.password,userArray[0].pass,(err,result)=>{
                    console.log(result);
                    if(result==true)
                    {
                        //create and send json token
                        const signedtoken=jwt.sign({username:userArray[0].user},secretkey,{expiresIn:"7d"});
                        console.log(signedtoken);
                        //send signed token as response after successful login 
                        res.json({message:"success",token:signedtoken, data:userArray});
                    }
                    else{
                        res.json({message:"Invaild Password"})
                    }
                })
            }
        })
    });
   
                    
studentroutes.post('/specificstnmarks',checkAuthorization,(req,res)=>{
    dbo=getDb();
    console.log(req.body)
    dbo.collection('postingmarkscollection').find({no:{$eq:req.body.rno}}).toArray((err,success)=>{
        if(err)
        {
            console.log('error in data reading');
            console.log(err);
        }
        else if(success.length==0)
        {
            res.json({"message":"no data found"});
            console.log(success);
        }
        else{
            console.log(success)
            res.json({"message":success});
           
        }
    })
})

//student route handlers
studentroutes.get('/marks',(req,res)=>{

    dbo=getDb();
    dbo.collection('postingmarkscollection').find().toArray((err,success)=>{
        if(err)
        {
            console.log("error in data reading");
            console.log(err);
        }
        else if(success.length==0)
        {
            res.json({"message":"no data found"});
        }
        else
        {
            res.json({"message":success});
        }
    })
    });
    //student route handlers
studentroutes.get('/notifications',(req,res)=>{

    dbo=getDb();
    dbo.collection('notificationscollection').find().toArray((err,success)=>{
        if(err)
        {
            console.log("error in data reading");
            console.log(err);
        }
        else if(success.length==0)
        {
            res.json({"message":"no data found"});
        }
        else
        {
            res.json({"message":success});
        }
    })
    });
    studentroutes.post('/specificstnattendence',checkAuthorization,(req,res)=>{
        dbo=getDb();
        console.log(req.body)
        dbo.collection('attendencecollection').find({no:{$eq:req.body.rno}}).toArray((err,success)=>{
            if(err)
            {
                console.log('error in data reading');
                console.log(err);
            }
            else if(success.length==0)
            {
                res.json({"message":"no data found"});
                console.log(success);
            }
            else{
                console.log(success)
                res.json({"message":success});
               
            }
        })
    })
    
    //student route handlers
studentroutes.get("/attendence",(req,res)=>{

    dbo=getDb();
    dbo.collection("attendencecollection").find().toArray((err,dataArray)=>{
        if(err)
        {
            console.log("error in database server");
            console.log(err);
        }
        
        else
        {
            res.json({"message":dataArray})
        }
    })
    });
    studentroutes.post('/specificstnfees',checkAuthorization,(req,res)=>{
        dbo=getDb();
        console.log(req.body)
        dbo.collection('feescollection').find({no:{$eq:req.body.rno}}).toArray((err,success)=>{
            if(err)
            {
                console.log('error in data reading');
                console.log(err);
            }
            
            else{
                console.log(success)
                res.json({message:success});
               
            }
        })
    })
    
    //student route handlers
studentroutes.get('/fee',(req,res)=>{

    dbo=getDb();
    dbo.collection('feescollection').find().toArray((err,success)=>{
        if(err)
        {
            console.log("error in data reading");
            console.log(err);
        }
        else if(success.length==0)
        {
            res.json({"message":"no data found"});
        }
        else
        {
            res.json({"message":success});
        }
    })
    });
   /* studentroutes.post("/leave",(req,res)=>{
        console.log(req.body);
        dbo=getDb();
        dbo.collection("leavecollection").insert(req.body,(err,success)=>{
if(err)
{
    console.log('error in adding leave request');
}
else{
    res.json({message:"Request sent successfully"});
}
        })
    })
    */
   studentroutes.post('/specificleaverequest',checkAuthorization,(req,res)=>{
       dbo=getDb()
       dbo.collection("responce").find({rno:{$eq:req.body.rno}}).toArray((err,dataArray)=>{
           if(err)
           {
               console.log("error in reading data")
           }
           else{
               console.log(dataArray)
               res.json({message:dataArray})
           }
       })
   })
               studentroutes.post('/savereq',(req,res,next)=>{
                    console.log(req.body);
                    dbo=getDb();
                    if(req.body=={})
                    {
                res.json({message:"server did not receive data"})
                    }
                    else{
                        dbo.collection("leavecollection").insertOne(req.body,(err,dataArray)=>{
                            if(err){
                                next(err)
                            }
                            else{
                                dbo.collection("responce").deleteOne({rno:{$eq:req.body.rno}},(err,dataArray)=>{
                                    if(err)
                                    {
                                        next(err)
                                    }
                                    else{
                                        res.json({message:"successfully inserted"})
                                    }
                                })
                            }
                        })
                    }
                    });
studentroutes.post('/create',(req,res)=>{
    res.json({message:"student create working"});
})
studentroutes.put('/update',(req,res)=>{
    res.json({message:"student update working"});
})
studentroutes.delete('/delete',(req,res)=>{
    res.json({message:"student delete working"});
});
module.exports=studentroutes;