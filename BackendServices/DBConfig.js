const mc=require("mongodb").MongoClient;

const url="mongodb://nithyasree:dinesh@cluster0-shard-00-00-npkk9.mongodb.net:27017,cluster0-shard-00-01-npkk9.mongodb.net:27017,cluster0-shard-00-02-npkk9.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
var dbo;

function initDb()
{
    mc.connect(url,{useNewUrlParser:true},(err,db)=>{
        if(err)
        {
            console.log("error in connecting to database")
        }
        else{
            console.log("database connected");
            dbo=db.db("sampleDatabase")
        }
    });
}

function getDb()
{
    
    return dbo;
}
module.exports={
    initDb,
    getDb
};